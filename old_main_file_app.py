from flask import Flask, jsonify
from os import environ
from flask_pymongo import PyMongo
import seeds

app = Flask(__name__)
app.config["MONGO_URI"] = environ['MONGO_URL']
mongo = PyMongo(app)
seeds.seeddatabase(mongo)

@app.route('/')
def foo():
  return jsonify({ 'msg': 'Cool' })

@app.route("/markers")
def markers():
    markers = [marker for marker in mongo.db.marker.find({})]
    results = []
    for marker in markers:
          marker['_id'] = str(marker['_id'])
          marker['parkinglot_id'] = str(marker['parkinglot_id'])
          results.append(marker)
    return jsonify(results)

app.run(host=environ['HOST'], port=environ['PORT'])
