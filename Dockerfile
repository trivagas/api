FROM python:3.7-alpine

ENV PORT=5000 \
    HOST=0.0.0.0

WORKDIR /usr/src/app

EXPOSE ${PORT}

RUN apk add --update build-base libffi-dev

COPY requirements.txt ./

RUN pip install --upgrade pip && \
    pip install -r requirements.txt

COPY . ./

ENTRYPOINT [ "python", "index.py" ]
