from random import random
from faker import Faker
from faker.providers import address, color, date_time, geo, company

fake = Faker(locale="pt_BR")

fake.add_provider(address)
fake.add_provider(color)
fake.add_provider(date_time)
fake.add_provider(geo)
fake.add_provider(company)

class ParkinglotFactory():
  @classmethod
  def new_parkinglot(self):
    pl = {
      'name': fake.company(),
      'address': fake.street_address(),
      'opening_hours': fake.time(pattern="%H:%M"),
      'pricingTable': [
        { 'order': 1, 'label': '1ª hora', 'price': 'R$ 8,00' },
        { 'order': 2, 'label': '2ª hora', 'price': '+ R$ 4,00' },
        { 'order': 3, 'label': 'Demais (p/h)', 'price': '+ R$ 2,00' },
        { 'order': 4, 'label': 'Pernoite', 'price': 'R$ 15,00' },
        { 'order': 5, 'label': 'Diária', 'price': 'R$ 15,00' },
        { 'order': 6, 'label': 'Mensalista', 'price': 'R$ 270,00' }
      ]
    }

    return pl

class MarkerFactory():
  @classmethod
  def new_marker(self):
    # memorial da america latina
    mem_lat, mem_lng = -23.521532, -46.664602

    # aquario de sao paulo
    aqu_lat, aqu_lng = -23.593921, -46.613796

    lat = (mem_lat - aqu_lat) * random() + aqu_lat
    lng = (aqu_lng - mem_lng) * random() + mem_lng

    mk = {
      'label': fake.color_name(),
      'parkinglot_id': '',
      'position': {
        'lat': float(lat),
        'lng': float(lng)
      }
    }

    return mk

def seed():
  pls = []
  for _ in range(50):
    parkinglot = ParkinglotFactory.new_parkinglot()
    pls.append(parkinglot)

  mks = []
  for _ in range(50):
    marker = MarkerFactory.new_marker()
    mks.append(marker)

  return pls, mks
