import os
import json
import datetime

from bson.objectid import ObjectId
from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt
from seed import seed

class JSONEncoder(json.JSONEncoder):
  def default(self, o):
    if isinstance(o, ObjectId):
      return str(o)
    if isinstance(o, datetime.datetime):
      return str(o)
    return json.JSONEncoder.default(self, o)

app = Flask(__name__)
CORS(app)
app.config['MONGO_URI'] = os.environ.get('DB')
app.config['JWT_SECRET_KEY'] = os.environ.get('SECRET_KEY')
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)
app.config['CORS_HEADERS'] = 'Content-Type'
mongo = PyMongo(app)
flask_bcrypt = Bcrypt(app)
jwt = JWTManager(app)
app.json_encoder = JSONEncoder

if os.environ.get('ENV') == 'development':
  with app.app_context():

    if mongo.db.markers.count() == 0:
      parkinglots, markers = seed()

      results = mongo.db.parkinglots.insert_many(parkinglots)

      for i in range(len(markers)):
        markers[i]['parkinglot_id'] = results.inserted_ids[i]

      mongo.db.markers.insert_many(markers)

from app.controllers import *
