import os
import logger
from flask import request, jsonify
from app import app, mongo, flask_bcrypt, jwt
from bson.json_util import dumps
from bson import ObjectId
from app.schemas import validate_user
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, jwt_refresh_token_required, get_jwt_identity)

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
  __name__, filename=os.path.join(ROOT_PATH, 'output.log')
)

# Add admin user
with app.app_context():
  admin = mongo.db.users.find_one({'email': 'admin@trivagas.com'})
  if admin == None:
    data = {
      "name": "Admin",
      "email": "admin@trivagas.com",
      "password": "processinhodatrivago",
    }
    data['password'] = flask_bcrypt.generate_password_hash(data['password'])
    data['role'] = 'admin'
    mongo.db.users.insert_one(data)

@jwt.unauthorized_loader
def unauthorized_response(callback):
  return jsonify({
    'ok': False,
    'message': 'Missing Authorization Header'
  }), 401

@app.route('/auth', methods=['POST'])
def auth_user():
  ''' auth endpoint '''
  data = validate_user(request.get_json())
  if data['ok']:
    data = data['data']
    user = mongo.db.users.find_one({'email': data['email']})
    LOG.debug(user)
    if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
      del user['password']
      access_token = create_access_token(identity=data)
      refresh_token = create_refresh_token(identity=data)
      user['token'] = access_token
      user['refresh'] = refresh_token
      user['id'] = str(user['_id'])
      # Some users were registered without role
      if not 'role' in user:
        user['role'] = 'user'
      return jsonify({'ok': True, 'data': user}), 200
    else:
      return jsonify({'ok': False, 'message': 'invalid username or password'}), 401
  else:
    return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400

@app.route('/register', methods=['POST'])
def register():
  ''' register user endpoint '''
  data = validate_user(request.get_json())
  user = mongo.db.users.find_one({'email': data['data']['email']})
  if user == None:
    if data['ok']:
      data = data['data']
      data['password'] = flask_bcrypt.generate_password_hash(
          data['password'])
      data['role'] = 'user'
      mongo.db.users.insert_one(data)
      return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
    else:
      return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400
  else:
      return jsonify({'ok': False, 'message': 'Email already exists'}), 401
@app.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
  ''' refresh token endpoint '''
  current_user = get_jwt_identity()
  ret = {
    'token': create_access_token(identity=current_user)
  }
  return jsonify({'ok': True, 'data': ret}), 200

@app.route('/user', methods=['GET', 'DELETE', 'PATCH'])
@jwt_required
def user():
  ''' route read user '''
  if request.method == 'GET':
    query = request.args
    data = mongo.db.users.find_one({"_id": ObjectId(query['id'])})
    del data['password']
    del data['_id']
    return jsonify({'ok': True, 'data': data}), 200

  data = request.get_json()
  if request.method == 'DELETE':
    if data.get('email', None) is not None:
      db_response = mongo.db.users.delete_one({'email': data['email']})
      if db_response.deleted_count == 1:
        response = {'ok': True, 'message': 'record deleted'}
      else:
        response = {'ok': True, 'message': 'no record found'}
      return jsonify(response), 200
    else:
      return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400

  if request.method == 'PATCH':
    user = mongo.db.users.find_one({'email': data['query']['email']})
    LOG.debug(user)
    if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
      if data.get('query', {}) != {}:
        data['payload']['password'] = flask_bcrypt.generate_password_hash(
          data['payload']['password'])
        mongo.db.users.update_one(
          data['query'], {'$set': data.get('payload', {})})
        return jsonify({'ok': True, 'message': 'record updated'}), 200
      else:
        return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400
    else:
      return jsonify({'ok': False, 'message': 'invalid password'}), 401