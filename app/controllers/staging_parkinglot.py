import os
import logger
from app import app, mongo, jwt
from flask import request, jsonify
from flask_jwt_extended import jwt_required
from app.schemas import validate_parkinglot
from bson.json_util import dumps

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
  __name__, filename=os.path.join(ROOT_PATH, 'output.log')
)

@jwt.unauthorized_loader
def unauthorized_response(callback):
  return jsonify({
    'ok': False,
    'message': 'Missing Authorization Header'
  }), 401

@app.route('/staging_parkinglot', methods=['POST'])
@jwt_required
def stage_parkinglot():
  ''' staging parking lot endpoint '''
  data = validate_parkinglot(request.get_json())
  if data['ok']:
    data = data['data']
    mongo.db.staged_parkinglots.insert_one(data)
    return jsonify({'ok': True, 'message': 'Parking lot staged successfully!'}), 200
  else:
    return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400
