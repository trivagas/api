import os
import logger
from flask import request, jsonify
from app import app, mongo
from bson.json_util import dumps

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
  __name__, filename=os.path.join(ROOT_PATH, 'output.log')
)

def get_markers():
  data = dumps(mongo.db.markers.find({}))
  return jsonify(data), 200

@app.route('/markers', methods=['GET'])
def markers():
  # to use GET parameters, such
  # /?key=value&key2=value2
  # query = request.args
  return get_markers()
