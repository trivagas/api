import os
import logger
from flask import request, jsonify, send_from_directory
from app import app, mongo
from bson.objectid import ObjectId
from bson.json_util import dumps

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
  __name__, filename=os.path.join(ROOT_PATH, 'output.log')
)

def get_parkinglots():
  data = dumps(mongo.db.parkinglots.find({}))
  return jsonify(data), 200, { 'Access-Control-Allow-Origin': os.environ.get('CLIENT_URL') }

def get_parkinglot(id):
  raw = mongo.db.parkinglots.find_one({ '_id': ObjectId(id) })
  data = dumps(raw)
  return jsonify(data), 200, { 'Access-Control-Allow-Origin': os.environ.get('CLIENT_URL') }

def get_parkinglot_image(id):
  return send_from_directory('images', 'cbk.jpeg')


### routes

@app.route('/parkinglots', methods=['GET'])
def parkinglots():
  return get_parkinglots()

@app.route('/parkinglots/<string:id>', methods=['GET'])
def parkinglot(id):
  return get_parkinglot(id)

@app.route('/parkinglots/<string:id>/image', methods=['GET'])
def parkinglot_image(id):
  return get_parkinglot_image(id)