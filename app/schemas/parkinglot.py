from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError

parkinglot_schema = {
  "type": "object",
  "properties": {
    "name": { "type": "string" },
    "address": { "type": "string" },
    "zipCode": { 
      "type": "string",
      "minLength": 9 
    },
    "pricingTable": { "type": "string"},
  },
  "required": ["name", "address", "zipCode", "pricingTable"],
  "additionalProperties": False
}

def validate_parkinglot(data):
  try:
    validate(data, parkinglot_schema)
  except ValidationError as e:
    return { 'ok': False, 'message': e }
  except SchemaError as e:
    return { 'ok': False, 'message': e }
  return { 'ok': True, 'data': data }
