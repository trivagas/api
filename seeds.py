from models.Parkinglot import ParkingLot
from models.Markers import Markers

def seeddatabase(mongo):
    park1 = ParkingLot('stapar','rua x','','12:00 - 18:00')
    park2 = ParkingLot('Parkat','rua y','','8:00 - 18:00')
    _id1 = mongo.db.parkinglot.insert_one(park1.__dict__)
    _id2 = mongo.db.parkinglot.insert_one(park2.__dict__)
    marker1 = Markers(park1.name, 32, 21, str(_id1.inserted_id))
    marker2 = Markers(park2.name, 32, 21, str(_id2.inserted_id))
    mongo.db.marker.insert_one(marker1.__dict__)
    mongo.db.marker.insert_one(marker2.__dict__)